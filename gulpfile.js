'use strict';
/*
PROBLEMS:
  gulp-imagemin has 9 vulnerabilities

TO DO:
 cleancss
 purgecss
cachebust

*/

const { watch, series, parallel, src, dest } = require('gulp');

// Importing all the Gulp-related packages we want to use
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const babel = require('gulp-babel');
const browsersync = require('browser-sync').create();
const sourcemaps = require('gulp-sourcemaps');
const imagemin = require('gulp-imagemin'); 
const stylelint = require('gulp-stylelint');


const path = {
  public: {
    src: './src/*.html',
    dist: './dist/'
  },

  styles: {
    src: './src/styles/**/*.scss',
    dist: './dist/styles/'
  },

  scripts: {
    src: './src/scripts/*.js',
    dist: './dist/scripts/'
  },

  images: {
    src: './src/images/*.{png,jpg,jpeg,gif,svg}',
    dist: './dist/images/'
  }
}


function publicFiles() {
  return src(path.public.src)
    .pipe(dest(path.public.dist))
    .pipe(browsersync.stream());
}

function styles() {
  return src(path.styles.src)
    .pipe(stylelint({
      failAfterError: false,
      reporters: [
        {formatter: 'verbose', console: true}
      ]
    }))
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([autoprefixer({
      cascade: false,
      overrideBrowserslist: ['last 2 versions']
    })
    , cssnano({
      discardComments: {
        removeAll: true,
      },
      discardDuplicates: true,
      discardEmpty: true,
      minifyFontValues: true,
      minifySelectors: true,
    })
    ]))
    .pipe(sourcemaps.write('.')) 
    .pipe(dest(path.styles.dist))
    .pipe(browsersync.stream());
}


function stylesBuild() {
  return src(path.styles.src)
    .pipe(stylelint({
      failAfterError: null,
      reporters: [
        {formatter: 'verbose', console: true}
      ]
    }))
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([autoprefixer({
      cascade: false,
      overrideBrowserslist: ['last 2 versions']
    })
    , cssnano({
      discardComments: {
        removeAll: true,
      },
      discardDuplicates: true,
      discardEmpty: true,
      minifyFontValues: true,
      minifySelectors: true,
    })
    ]))
    .pipe(dest(path.styles.dist))
    .pipe(browsersync.stream());
}

function scripts() {
  return src([path.scripts.src
    //,'!' + 'includes/js/jquery.min.js', // to exclude any specific files
  ])
    .pipe(babel({
      presets: ['@babel/preset-env']
    }))
    .pipe(concat('main.js'))
    .pipe(uglify().on('error', console.error))
    .pipe(dest(path.scripts.dist))
    .pipe(browsersync.stream());
}

function images() {
  return src(path.images.src)
    .pipe(imagemin({ // 9 VULNERABILITIES
      progressive: true,
      interlaced: true,
      pngquant: true,
      optimizationLevel: 7,
      verbose: true,
      use: []
    }))
    .pipe(dest(path.images.dist))
    .pipe(browsersync.stream());
}


function watchDev() {
  browsersync.init(null, {
    browser: "google-chrome-stable",
    //proxy: '127.0.0.1:8889',
    server: {
      baseDir: "./dist"
    },
    port: 8080,
    open: 'external',
    reloadOnRestart: true,
    notify: true,
  });

  watch(path.public.src, series(parallel(publicFiles))).on('change', browsersync.reload);
  watch(path.styles.src, series(parallel(styles)));
  watch(path.scripts.src, series(parallel(scripts))).on('change', browsersync.reload);
  watch(path.images.src, series(parallel(images)));
}

exports.default = series(
  parallel(publicFiles, styles, scripts, images),
  // cacheBustTask,
  watchDev
);

exports.build = series(
  parallel(publicFiles, stylesBuild, scripts, images)
);